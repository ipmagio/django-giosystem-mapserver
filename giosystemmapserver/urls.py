from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^latest$', views.latest_products, name='latest_products')
)
