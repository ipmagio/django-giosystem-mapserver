import os

from django.conf import settings
from django.http import HttpResponse
import mapscript

import djangomapserver.ows as ows
# could generate the palletes from a djangomapserver model


def latest_products(request):
    mapfile = _build_latest_mapfile(request.path)
    mapfile.save("/home/ricardo/Desktop/teste.map")
    return _handle_mapserver_output(request, mapfile)


# def all_products(request, product, timeslot):
#     """
#     Serve a map for any of the GIO products.
# 
#     This view will fetch the products from the filesystem(s)
#     and therefore needs acess to giosystemcore.settings
#     """
# 
#     pass


def _handle_mapserver_output(request, mapfile):
    result, ct = ows.process_request(request, mapfile)
    content_type = ows.get_content_type(ct)
    response = HttpResponse(content_type=content_type)
    response.write(result)
    return response


def _build_latest_mapfile(request_path):
    """
    Create a custom mapfile for displaying the latest GIO products.
    """

    projection_code = settings.OWS["projection"]
    m = mapscript.mapObj()
    m.name = settings.OWS["ows"]["title"].replace(" ", "_")
    m.setProjection("init=epsg:{}".format(projection_code))
    m.shapepath = settings.OWS["shape_path"]
    m.units = mapscript.MS_DD
    for k, v in _get_metadata(request_path).iteritems():
        m.setMetaData(k, v)
    m.imagetype = "png"
    m.extent = mapscript.rectObj(*settings.OWS["extent"])
    m.setSize(*settings.OWS["size"])
    m.imageColor = mapscript.colorObj(*settings.OWS["image_color"])
    for name, path, palette in _get_dynamic_layers():
        layer = _build_layer(name, path, palette)
        m.insertLayer(layer)
    return m


def _get_metadata(request_path, wms=True, wcs=True):
    online_resource = "/".join((settings.OWS["ows"]["ows_onlineresource"],
                               request_path))
    meta = dict()
    if wms:
        meta.update({
            "wms_title": settings.OWS["ows"]["title"],
            "ows_onlineresource": online_resource,
            "wms_srs": projection,
            "ows_enable_request": settings.OWS["ows"]["ows_enable_request"],
            "wms_encoding": "utf-8",
        })
    if wcs:
        meta.update({
            "wcs_label": settings.OWS["ows"]["title"],
        })
    return meta


def _build_layer(name, path, palette):
    layer = mapscript.layerObj()
    layer.name = name
    layer.status = mapscript.MS_ON
    layer.template = "templates/blank.html"
    layer.dump = mapscript.MS_TRUE
    layer.setProjection(settings.OWS["projection"])
    layer.meta.set("wms_title", name)
    layer.meta.set("wms_srs", settings.OWS["projection"])
    layer.meta.set("wms_include_items",
                   settings.OWS["ows"]["ows_include_items"])
    layer.meta.set("gml_include_items",
                   settings.OWS["ows"]["gml_include_items"])
    layer.meta.set("wcs_label", name)
    layer.meta.set("wcs_rangeset_name", "range 1")
    layer.meta.set("wcs_rangeset_label", "my label")
    layer.metadata = layer_meta
    layer.data = path
    layer.type = mapscript.MS_LAYER_RASTER
    # add the pallete stuff here
    return layer


def _get_dynamic_layers():
    layers = []
    directory = settings.LAYERS_DIR
    for filename in os.listdir(directory):
        path = os.path.join(directory, filename)
        fname, extension = os.path.splitext(filename)
        path_parts = fname.split('_')
        product = path_parts[2]
        dataset = '_'.join(path_parts[7:])
        layers.append(
            ('{}_{}'.format(product, dataset), path, [])
        )
    return layers
