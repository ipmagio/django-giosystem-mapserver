This is a django app that generates mapserver OGC services for giosystem

Installation
============

0. Create a virtualenv to hold the installation (This step is
   optional, but highly reccommended).

   .. code:: bash

      virtualenv venv
      source venv/bin/activate

#. Install Mapserver and the mapscript python pacakge system-wide

#. Link the mapscript package into the virtualenv

#. Install this repository using pip

   .. code:: bash

      pip install git+https://github.com/ricardogsilva/django-giosystem-mapserver.git#egg=django-giosystem-mapserver
